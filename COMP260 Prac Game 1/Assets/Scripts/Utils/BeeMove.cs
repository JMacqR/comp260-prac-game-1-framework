﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

	public float speed = 4.0f; // metres per second
	public float turnSpeed = 180.0f; // degrees per second
	public Transform target;
	public Transform target2;
	public Vector2 heading = Vector3.right;

	// Update is called once per frame
	void Update () {
		// get the vector from the bee to the target
		Vector2 direction = target.position - transform.position;

		Vector2 direction2 = target2.position - transform.position;

		//Vector2 direction1 = target.position - GameObject.FindGameObjectWithTag("Player2").transform.position;

		//if (Vector2.Distance (GameObject.FindGameObjectWithTag("Player").transform.position, target.position) < 1) {
		//	Debug.Log ("Less than one.");
		//} else {
		//	Debug.Log ("Greater than one.");
		//} Closest one!

		//var distance = Vector3.Distance (GameObject.FindGameObjectWithTag("Player"),GameObject.FindGameObjectWithTag("Player2"));
			//(GameObject.FindWithTag("Player").transform, GameObject.FindWithTag("Player2").transform);

		//var heading = target.position - transform.position;
		//var distance = heading.magnitude;
		//if (heading.magnitude < maxRange * maxRange) {
			// Target is within range.
		//}
		//heading.y = 0;

	//if (heading.magnitude (transform.position,target.position)) {

		//direction.sqrMagnitude ();

		//if (transform == transform) {
			//return;

		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		// determine following either one player or the other
		if (direction.magnitude < direction2.magnitude) {

			//if (direction.IsOnLeft (GameObject.FindGameObjectsWithTag("Player,Player2").transform.position)) {
			// turn left or right
			if (direction.IsOnLeft (heading)) {
				// target on left, rotate anticlockwise
				heading = heading.Rotate (angle);
			} else {
				// target on right, rotate clockwise
				heading = heading.Rotate (-angle);
			}
			// following the second player if closest
		} else {
			if (direction2.IsOnLeft (heading)) {
				heading = heading.Rotate (angle);
			} else {
				heading = heading.Rotate (-angle);
			}
		}
			

		//}

		//if (direction.magnitude > transform.Translate){

		transform.Translate (heading * speed * Time.deltaTime);

		//heading = 
		//Vector2.Distance (transform.position, target.position);

	}

	void OnDrawGizmos() {
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay (transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Gizmos.DrawRay (transform.position, direction);
	}


	//void Update () {
	//	heading = Vector2.Distance (transform.position, target.position);
	//}
}

//}