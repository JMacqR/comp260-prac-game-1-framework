﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {
	//CharacterController playerOne;
	//CharacterController playerTwo;
	public Transform playerOne;
	public Transform playerTwo;

	// Use this for initialization
	void Start () {
		//playerOne = GameObject.FindGameObjectWithTag("Player1").GetComponent<CharacterController>();
		//playerTwo = GameObject.FindGameObjectWithTag("Player2").GetComponent<CharacterController>();
		//playerOne = transform;
		//playerTwo = transform;
	}
	// outer class definition omitted

	public Vector2 move;
	public Vector2 velocity; //in metres per second
	public Vector2 move2;
	public Vector2 velocity2; //in metres per second
	public float maxSpeed = 5.0f;

	// Update is called once per frame
	void Update () {
		// get the input values
		if (playerOne) {
			Vector2 direction;
			direction.x = Input.GetAxis ("Horizontal");
			direction.y = Input.GetAxis ("Vertical");

			//scale the velocity by the frame duration
			Vector2 velocity = direction * maxSpeed;

			// move the object
			transform.Translate(velocity * Time.deltaTime);
		}

		if (playerTwo) {
			Vector2 direction2;
			direction2.x = Input.GetAxis ("Horizontal2");
			direction2.y = Input.GetAxis ("Vertical2");

			Vector2 velocity2 = direction2 * maxSpeed;

			transform.Translate(velocity2 * Time.deltaTime);
		}
			
	}
		
}
